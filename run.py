import uvicorn
from app.main import app
from app.config import Config

if __name__ == '__main__':
    """
        Run a ASGI server on address and port defined in Config
    """
    uvicorn.run(app, host=Config.HOST, port=Config.PORT)