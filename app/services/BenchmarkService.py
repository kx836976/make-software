import re
from evalplus.data import get_human_eval_plus
from metagpt.roles import Architect, Engineer, ProductManager, ProjectManager, QaEngineer
from metagpt.team import Team
from fastapi.responses import StreamingResponse
from evalplus.data import get_human_eval_plus, write_jsonl
import os

class BenchmarkService:
    """
    BenchmarkService class provides static methods to simulate a team working on a software project,
    run benchmarks using HumanEvalPlus, and extract relevant data from problem statements.
    """
    @staticmethod
    async def startup(idea: str):
        """
        Simulates a project execution with a team working on the provided idea.

        Args:
            idea (str): The project idea or problem prompt that will be used as a base for the project execution.
        
        Sets up a team with various roles including Product Manager, Architect, Project Manager,
        Engineers (with specific constraints), and QA Engineer. It then runs a project simulation.
        """

        # Custom constraints for engineer to run benchmarks smoothly
        engineer_constrainst = (
            "do not use classes or OOP structure"
            "complete the provided function definition"
            "the code should conform to standards like google-style and be modular and maintainable. "
            "Use same language as user requirement"
        )
        company = Team()
        
        # Assemble Team Members
        company.hire(
            [
                ProductManager(),
                Architect(),
                ProjectManager(),
                Engineer(n_borg=5, use_code_review=True, constraints = engineer_constrainst),
                QaEngineer()
            ]
        )

        # Run the project
        company.invest(investment=3.0)
        company.run_project(idea=idea)

        # Run 5 times
        await company.run(n_round=5)

    @staticmethod
    def get_first_number(string):
        """
        Extracts the first number from a given string using regex.

        Args:
            string (str): Input string from which to extract the first number.

        Returns:
            int: The first number found in the string, or None if no number is found.
        """
        match = re.search(r'\d+', string)
        if match:
            return int(match.group())
        return None

    @staticmethod
    async def run_benchmark():
        """
        Runs a benchmark using problems from the HumanEvalPlus dataset.
        
        Iterates through tasks, starts a project simulation for each, and returns
        results via a streaming response.

        Returns:
            StreamingResponse: A streaming response containing the benchmark results.
        """
        results = []
        for task_id, problem in get_human_eval_plus().items():
            id = BenchmarkService.get_first_number(task_id)
            res = await BenchmarkService.startup("Do not use any class structure (OOP) to build. " + problem['prompt'])
            return StreamingResponse(res.aiter_bytes(), status_code=res.status_code, headers=(res.headers))
            
        return results
    
    @staticmethod
    def evaluate(folder_path, output_name="sample.jsonl"):
        """
        This function evaluates the contents of a given folder containing Python files.
    
        Args:
            folder_path (str): The path to the folder containing Python files for evaluation.

        This function scans for Python files named "HumanEval<index>.py" (where <index> is 0 to 163),
        reads their contents, and stores them in a dictionary. If a file is not available, it is added to
        the `not_available` list. Finally, it creates a list of samples, each containing a task ID and the
        associated solution, which is then written to a JSONL file.
        """
        par_path = folder_path
        GENERATED_CODE = {}
        not_available=[]
        for i in range(0, 164):
            try:
                GENERATED_CODE['HumanEval/' + str(i)] = ''
                files = os.listdir(par_path)
                python_files = [file for file in files if file.endswith('.py')]
                file_name = "HumanEval" + str(i) + ".py"
                
                if file_name in python_files:
                    with open(par_path + "/" + file_name, 'r') as f:
                        GENERATED_CODE['HumanEval/' + str(i)] = f.read()
                else:
                    not_available.append(file_name)

            except FileNotFoundError as e:
                continue

        samples = [
            dict(task_id=task_id, solution=GENERATED_CODE[task_id])
            for task_id, problem in get_human_eval_plus().items()
        ]

        write_jsonl(output_name, samples)
        return "Success"
