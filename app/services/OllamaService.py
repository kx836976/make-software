# app/services/ollama.py
import httpx
from fastapi import Request
from fastapi.responses import StreamingResponse
from app.config import Config
import logging

class OllamaService:
    """
    OllamaService class provides two main static methods:
    1. select_ollama_model: Modifies the request body and headers to select appropriate models based on the content.
    2. relay_request: Relays the incoming request to the Ollama API, adjusting the request and headers as needed.
    """

    @staticmethod
    def select_ollama_model(req: bytes, headers):
        """
        Modifies the request body to select a different model for 'engineer' tasks and adjusts the content length header.

        Args:
            req (bytes): The original request body in bytes.
            headers (dict): The headers of the request to be forwarded.

        Returns:
            Tuple: Updated request body and headers.

        This method checks if the request text contains the word 'engineer'. If found, it switches the model from 
        'mistral-nemo:latest' to 'deepseek-coder-v2:latest'. It also adjusts the 'content-length' header to reflect
        the updated size of the request.
        """
        
        txt = req.decode("utf-8")  
        # Check if the text contains the word "engineer" to modify the model
        if "engineer" in txt.lower():
            
            mod1 = Config.NON_TECHNICAL_MODEL  # Original model
            mod2 = Config.TECHNICAL_MODEL  # New model to switch to
            
            print(f"Model changing from {mod1} to {mod2}")
            
            # Adjust the 'content-length' header to account for the change in model names
            headers['content-length'] = str(int(headers['content-length']) + (len(mod2) - len(mod1)))
            
            # Replace the old model with the new one in the request body
            txt = txt.replace(mod1, mod2)

        # Encode the modified text back to bytes and return the updated request and headers
        return txt.encode("utf-8"), headers

    @staticmethod
    async def relay_request(path: str, request: Request):
        """
        Relays an incoming request to the Ollama API, modifying the request body and headers as necessary.

        Args:
            path (str): The API endpoint path to forward the request to.
            request (Request): The incoming FastAPI request object.

        Returns:
            StreamingResponse: A streaming response containing the response from the Ollama API.

        This method forwards either a GET or POST request to the Ollama API endpoint. It adjusts the request headers
        and body if needed (using select_ollama_model), and then forwards the request using httpx AsyncClient.
        """
        logging.info("Received prompt from Client Frameword.")
        
        ollama_end_point = f"{Config.OLLAMA_BASE_URL}/api/{path}"
        
        # Prepare the headers for the upstream request, excluding the 'host' header
        headers = {key: value for key, value in request.headers.items() if key.lower() != 'host'}
        
        # Retrieve the request body asynchronously
        request_body = await request.body()

        # Optionally modify the request body and headers based on the content (model selection)
        request_body, headers = OllamaService.select_ollama_model(request_body, headers)
        
        logging.info("Router operations completed")

        # Use an asynchronous HTTP client to forward the request to the Ollama API
        async with httpx.AsyncClient() as client:
            if request.method == "GET":
                upstream_response = await client.get(ollama_end_point, headers=headers, timeout=20)
            elif request.method == "POST":
                upstream_response = await client.post(ollama_end_point, headers=headers, data=request_body, timeout=20)
                
        logging.info("Interacting with concerned LLM model")

        # Return the response from the Ollama API as a streaming response
        return StreamingResponse(
            upstream_response.aiter_bytes(), 
            status_code=upstream_response.status_code, 
            headers=dict(upstream_response.headers)
        )
