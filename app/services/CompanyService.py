import re
from evalplus.data import get_human_eval_plus
from metagpt.roles import Architect, Engineer, ProductManager, ProjectManager, QaEngineer
from metagpt.team import Team

class CompanyService:
    @staticmethod
    async def startup(idea: str):
        """
        Simulates a project execution with a team working on the provided idea.

        Args:
            idea (str): The project idea or problem prompt that will be used as a base for the project execution.
        
        Sets up a team with various roles including Product Manager, Architect, Project Manager,
        Engineers (with specific constraints), and QA Engineer. It then runs a project simulation.
        """
        company = Team()
        company.hire(
            [
                ProductManager(),
                Architect(),
                ProjectManager(),
                Engineer(n_borg=5, use_code_review=True),
                QaEngineer(),
            ]
        )
        company.invest(investment=3.0)
        company.run_project(idea=idea)

        await company.run(n_round=5)

    @staticmethod
    async def start(idea):
        """
            start a company with an idea
            
            Args:
                idea (str): The project idea or problem prompt that will be used as a base for the project execution.
        """
        await CompanyService.startup(idea)
