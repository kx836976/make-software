class Config:
    OLLAMA_BASE_URL = "http://127.0.0.1:11434"
    HOST = "0.0.0.0"
    PORT = 5000
    NON_TECHNICAL_MODEL = "gemma2:27b"
    TECHNICAL_MODEL = "granite-code:34b"