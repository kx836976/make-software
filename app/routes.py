from fastapi import APIRouter, Request
from app.services.CompanyService import CompanyService
from app.services.BenchmarkService import BenchmarkService
from app.services.OllamaService import OllamaService

router = APIRouter()

@router.api_route("/api/{path:path}", methods=['GET', 'POST'])
async def relay_stream(path: str, request: Request):
    return await OllamaService.relay_request(path, request)

@router.api_route("/testConnection/", methods=['GET', 'POST'])
async def hello():
    return "hello world"

@router.api_route("/benchmark/", methods=["GET"])
async def benchmark():
    return await BenchmarkService.run_benchmark()

@router.api_route("/makesoftware/", methods=['GET'])
async def makesoftware(message):
    return await CompanyService.start(message)

@router.api_route("/evaluate", methods=['GET'])
def evaluate():
    return BenchmarkService.evaluate("C:/Users/kx836976/human_eval_deepseekcode_only", "samples_eval_deepseekcode_only.jsonl")