import httpx
from fastapi import Request
from fastapi.responses import StreamingResponse
from app.config import Config

class OllamaService:
    @staticmethod
    def select_ollama_model(req: bytes, headers):
        txt = req.decode("utf-8")
        if "engineer" in txt:
            mod1 = "llama3.1:8b"
            mod2 = "codellama"
            headers['content-length'] = str(int(headers['content-length']) + (len(mod2) - len(mod1)))
            txt = txt.replace(mod1, mod2)
            print("coding model used")
        return txt.encode("utf-8"), headers
    
    @staticmethod
    async def relay_request(path: str, request: Request):
        ollama_end_point = f"{Config.OLLAMA_BASE_URL}/{path}"
        headers = {key: value for key, value in request.headers.items() if key.lower() != 'host'}
        request_body = await request.body()

        request_body, headers = OllamaService.select_ollama_model(request_body, headers)

        async with httpx.AsyncClient() as client:
            if request.method == "GET":
                upstream_response = await client.get(ollama_end_point, headers=headers)
            elif request.method == "POST":
                upstream_response = await client.post(ollama_end_point, headers=headers, data=request_body)

        return StreamingResponse(upstream_response.aiter_bytes(), status_code=upstream_response.status_code, headers=dict(upstream_response.headers))